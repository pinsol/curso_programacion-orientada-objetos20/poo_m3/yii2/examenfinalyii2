<?php

namespace app\controllers;

class PruebasController extends \yii\web\Controller
{   
    public $layout='pruebas';


    public function actionUno()
    {
	return 'Hola mundo';
    }

    public function actionDos()
    {
        $d = new \DateTime();
        $d->modify('+2 day');
        $d->add(new \DateInterval('PT10M'));
        
	return $d->format('y-m-d h:i:s');
    }

    public function actionTres()
    {
	return $this->render('tres');
    }
    
 
}
